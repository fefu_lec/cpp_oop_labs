#include <iostream>
#include <algorithm>
#include <random>


//row major
//idx = row_idx * cols_per_row + col_idx
// | R0 C0 | R0 C1 |...
//int arr[rows][cols]

//column major
//idx = col_idx * rows_per_col + row_idx
// | C0 R0 | C0 R1 |...
//int arr[cols][rows]

const int N = 10;

int main()
{
    int n;
    int arr_fixed[N];
    std::cin >> n;

    int* arr = new int[n];
/*
    std::random_device r;
    std::default_random_engine e1(r());
    std::uniform_int_distribution<int> uniform_dist(-5,5);

    std::generate(arr, arr + n, [&](){return uniform_dist(e1);});
    std::for_each(arr, arr + n, [](int a){std::cout << a  << " ";});
    std::cout << "\n";
    std::replace_if(arr, arr + n, [](int a){ return a < 0;}, 0);
    std::for_each(arr, arr + n, [](int a){std::cout << a  << " ";});
  */
    std::cout << sizeof( arr ) / sizeof(arr[0] ); // doesn't work for dynamic allocated arrays,
                                                  // only for fixed-size arrays (C-style arrays like int arr_fixed[N]),
                                                  // where N = 10
    delete[] arr;
    return 0;
}

