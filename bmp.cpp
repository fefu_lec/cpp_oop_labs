#include <fstream>
#include <cstdint>

int main()
{
    std::ofstream os("image.bmp", std::ios_base::binary
                                | std::ios_base::out);

    //std::uint8_t, std::uint16_t, std::uint32_t

    std::uint16_t n = 0x4D42;
    //FILE*
    os.write(reinterpret_cast<char*>(&n), sizeof(std::uint16_t));
    
    os.close();
    return 0;
}
