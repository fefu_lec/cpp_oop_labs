//g++ -std=c++23 bmp23.cpp -o bmp23 && ./bmp23
//g++-13+

#include <fstream>
#include <cstdint>
#include <bit>
int main()
{
    std::ofstream os("image.bmp", std::ios_base::binary
                                | std::ios_base::out);

    //std::uint8_t, std::uint16_t, std::uint32_t

    std::uint16_t n = 0x424D;
    std::uint16_t n_rev = std::byteswap(n);
    //FILE*
    os.write(reinterpret_cast<char*>(&n_rev), sizeof(std::uint16_t));
    
    os.close();
    return 0;
}
