#include <concepts>
#include <cmath>
#include <iostream>
#include <ctime>
#include <vector>

template<std::size_t N>
concept BeerAllowed = N >= 18;

template<std::size_t N>
concept AbcentAllowed = N >= 21;

class Item {
    public:
        unsigned get_barcode()
        {
            return 0;
        }
        std::tm get_expiration_date()
        {
            return std::tm();
        }
};
class Choco: public Item {
public:
    bool GMO = false;
};
template<class T>
concept Buyable = std::derived_from<T, Item> &&
    requires(T t)
    {
        { t.get_barcode() } -> std::same_as<unsigned>; 
        { t.get_expiration_date()} -> std::convertible_to<std::tm>;
        t.GMO;
    };

template<std::size_t Age>
class User
{
    public:
        constexpr std::size_t get_age()
        {
            return Age;
        }
};

class Store
{
    public:
        template<std::size_t Age, class T>
            requires Buyable<T>
        constexpr void Buy(T*)
        {
            if constexpr (AbcentAllowed<Age>)
                std::cout << "Abcent Allowed\n";
            if constexpr (BeerAllowed<Age>)
                std::cout << "Beer Allowed\n";

                std::cout << "Juice Allowed\n";
        }
        
    private:
        std::vector<Item*> m_items ;
};





int main()
{
    Item i;
    Choco c;
    Store s;
    User<30> u;
    s.Buy<u.get_age()>(&c);

    return 0;
}
