#include <iostream>
#include <string>
#include <complex>
#include <type_traits>
#include <concepts>

template <std::size_t N>
concept PositiveNumber = N > 0; //simple requirement

template<class T>
concept ComplexNumber = requires (T a)
{
    a.real();
    a.imag();
};

/*
template<class T>
    auto is_greater(T a, T b) -> std::enable_if<!std::is_same<T, std::complex<float>>::value, bool>::type 
{
    std::cout << "enable_if for non std::complex<float> numbers!\n";
    return a > b;
}
*/
struct RealNotImag
{
    void real(int){}
};

template<ComplexNumber T>
bool is_greater(T a, T b) 
{
    return a.real() > b.real(); 
}


int main()
{
    std::cout << std::boolalpha;

    std::complex<float> a;
    std::complex<float> b(-1, 0);

    std::cout << is_greater(a,b);
    //won't satisfy ComplexNumber concept
    //std::cout << is_greater(1,2);
    //std::cout << is_greater(RealNotImag{}, RealNotImag{});
    return 0;
}

