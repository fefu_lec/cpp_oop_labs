#include <iostream>
#include <fstream>

struct MyClass
{
  std::string name;
  int value;
};

std::ostream& operator<<(std::ostream& os, const MyClass& obj) 
{ 
    return os << "{\"" << obj.name << " - " << obj.value << "}"; 
} //Выводит в формате {"name" - value}

int main()
{
    MyClass c;
    c.name = "Alina";
    c.value = 5;
    std::ofstream fout("format.txt");

    fout << c << "\n";
    
    std::cout << c << "\n";

    fout.close();

    return 0;
}
