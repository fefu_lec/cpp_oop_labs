#include <iostream>

/*
        Programmer
      /             \
  Web                 DB
      \              /
         Fullstack
 */
class Programmer
{
    public: 
    Programmer()
    {
        std::cout << "Constructor Programmer;\n";
    }
    virtual void work() const
    {
        std::cout << "Programmer works;\n";
    }
    virtual ~Programmer()
    {
        std::cout << "Destructor Programmer;\n";
    }
};

class Web : public virtual Programmer
{
    public:
    Web()
    {
        std::cout << "Constructor Web;\n";
    }
    virtual void work() const
    {
        std::cout << "Create website;\n";
    }
    ~Web()
    {
        std::cout << "Destructor Web;\n";
    }
};
class DB : public virtual Programmer
{
    public:
    DB()
    {
        std::cout << "Constructor DB;\n";
    }
    virtual void work() const
    {
        std::cout << "Create Database;\n";
    }
   ~DB()
    {
        std::cout << "Destructor DB;\n";
    }
};
class Fullstack : public Web, public DB
{
    public:
    Fullstack()
    {
        std::cout << "Constructor Fullstack;\n";
    }
    virtual void work() const
    {
        std::cout << "Fullstack works;\n";
        Web::work();
    }
  
   ~Fullstack()
    {
        std::cout << "Destructor Fullstack;\n";
    }
};

int main()
{
    //a - b = -(b - a)
    //A* c = new C; //covariant
    //c->print();
    //delete c;
    //
   /*Programmer* p = new Programmer;
   Programmer* w = new Web;
   Programmer* d = new DB;
   
   p->work();
   w->work();
   d->work();
    
   delete p;
   delete w;
   delete d;
*/
   Programmer* f = new Fullstack;
   f->work();
   delete f;
   return 0;
}
