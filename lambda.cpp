#include <iostream>
#include <algorithm>
#include <vector>

const int N = 15;

struct GREAT
{
    constexpr bool operator()(const int& lhs, const int& rhs) const 
    {
        return lhs > rhs; // assumes that the implementation handles pointer total order
    }
};

int main()
{
int n = 5;
//([](int i){std::cout << "Lambda: " << i << "\n";})(5);
std::cout << ([n](int i) mutable
 {
    std::cout << "Lambda: " << i + N << "\n";
    n += 4;

    std::cout << "Lambda after change: " << i + n << "\n";
    return i + n + N;
 })(5);

GREAT g;
if (g(4,5))
    std::cout << "N after lambda: " << n << "\n";

std::vector<int> v {5,7,22,7,1};
std::sort(v.begin(), v.end());
std::for_each(v.begin(), v.end(), [](int a){std::cout << a  << " ";});

return 0;
}
