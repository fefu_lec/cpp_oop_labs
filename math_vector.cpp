#include <iostream>

template<int x, int y, int z>
struct Vector
{
    //vector product
    template<int x1, int y1, int z1>
    constexpr auto operator*(const Vector<x1, y1, z1>& v)
    {
        return Vector<y*z1 - z*y1, z*x1 - x*z1, x*y1 - y*x1>();
        /*
         * another approach - constant expressions:
         * constexpr int x2 = y * z1 - z * y1;
         * constexpr int y2 = z * x1 - x * z1;
         * constexpr int z2 = x * y1 - y * x1;
         * return Vector<x2, y2, z2>();
         * */
    }
    constexpr int get_x()
    {
        return x;
    }
    constexpr int get_y()
    {
        return y;
    }
    constexpr int get_z()
    {
        return z;
    }
};

int main()
{
    Vector<-1, 2, -3> a;
    Vector<0, -4, 1> b;

    auto N = a * b; //type: Vector<-10, 1, 4>
    std::cout << "Vector N {" << N.get_x() << "; " << N.get_y() << "; " << N.get_z() << "}\n";             
    return 0;
}

