#include <iostream>
#include <algorithm>
#include <numeric>

constexpr int N = 3;
constexpr int M = 3;

/*
os << *(new rect(12,22,22,33));
//%s/\v\*\(new (.+)\)/\1/gc
os << rect(12,22,22,33);

ost << *(new rect(12,22,22,33));
ost << *(new rect(12,22,22,33));
ost << *(new rect(12,22,22,33));
*/
int main()
{
    /*
            T** = new T*
            for...
                T* = new T

            for ...
                delete[] T*
            delete[] T**
     
     */

    int** matrix = new int*[N];
    for (int i = 0; i < N; ++i)
        matrix[i] = new int[M];

    
    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < M; ++j)
            matrix[i][j] = i;
       
    }


    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < M; ++j)
           std::cout <<  matrix[i][j];
       
    }

    std::sort(matrix, matrix + N, [=](int* a, int* b)
            {
                return std::accumulate(a, a + M, 0) > std::accumulate(b, b + M, 0); 
            });

    std::cout << "\n\n";

    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < M; ++j)
           std::cout <<  matrix[i][j];
       
    }

    for (int i = 0; i < M; ++i)
        delete[] matrix[i];
    delete[] matrix;
 
    return 0;
}
