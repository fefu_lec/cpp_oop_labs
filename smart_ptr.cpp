#include <memory>
#include <iostream>
#include <utility>

void Cfunc(int* a)
{
    std::cout << "func: " << *a << "\n";
}

void func(std::unique_ptr<int>& a)
{
    std::cout << "func: " << *a << "\n";
}

std::unique_ptr<int> ret_func()
{
    return std::make_unique<int>(5);
}

void move_ownership(std::unique_ptr<int> resource)
{
    if (resource)
        std::cout << "move_ownership: " << *resource << "\n";
    else
        std::cout << "no ownership" << "\n";
}

struct Node
{
    int data = 0;

};

int main()
{

    Node n;
    Node& p; 
    std::cout << n.next;

    auto p = std::make_unique<int>(5);
    func(p);
    std::cout << "Cfunc: ";
    Cfunc(p.get());

    auto res = ret_func();
    std::cout << "ret_func: " << *res << "\n";

    move_ownership(std::move(res));
    std::cout << "res: " << res << "\n";

    res = ret_func();
    std::cout << "ref_func: " << *res << "\n";

    move_ownership(std::unique_ptr<int>(res.release()));
    std::cout << "res: " << res << "\n";
    move_ownership(std::unique_ptr<int>(res.release()));
    std::cout << *res;
    return 0;
}
