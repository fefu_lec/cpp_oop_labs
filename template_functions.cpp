#include <iostream>
#include <string>

//1
template <class T>
T add (T a, T b)
{
    return a + b;
}

//2
template <class T>
T add(T* a, T* b)  
{
    return *a + *b; 
}

//3
template <>
std::string add<std::string>(std::string a, std::string b) //full specialization
{
    return a + b; 
}


int main()
{
    std::string a = "1", b = "2";
    std::cout << "Arguments: a = \"" << a <<"\", b = " << b << "\"\n";
    std::cout << "add<T>(T,T): " << add(std::stoi(a), std::stoi(b)) << "\n";
    //if call add("a", "b") arguments have type const char* so the 2nd realization will be called. 
    std::cout << "add<T>(T*,T*): " << add("a", "b") << "\n";
    std::cout << "add<std::string>(std::string,std::string): " << add(a, b) << "\n";
    return 0;
}
